var app = angular.module('app', ['ngSanitize', 'queryBuilder']);
app.controller('QueryBuilderCtrl', ['$scope', function ($scope) {
    var data = '{"operator": "AND","rules": []}';

    function asString(query) {
        if (!query) return "";
        for (var str = "(", i = 0; i < query.rules.length; i++) {
            i > 0 && (str += query.operator);
            str += query.rules[i].operator ?
                computed(query.rules[i]) :
                query.rules[i].field + " " + query.rules[i].condition + " " + query.rules[i].data;
        }

        return str + ")";
    }

    $scope.json = null;

    $scope.fields = [
        { name: 'Firstname' },
        { name: 'Lastname' },
        { name: 'Birthdate' },
        { name: 'City' },
        { name: 'Country' }
    ];

    $scope.query = JSON.parse(data);

    $scope.$watch('query', function (newValue) {
        $scope.output = asString(newValue);
    }, true);
}]);

var queryBuilder = angular.module('queryBuilder', []);
queryBuilder.directive('queryBuilder', ['$compile', function ($compile) {
    return {
        restrict: 'E',
        scope: {
            query: '=',
            fields: '='
        },
        templateUrl: 'angular-query-builder.html',
        compile: function (element, attrs) {
            var content, directive;
            content = element.contents().remove();
            return function (scope, element, attrs) {
                scope.operators = [
                    { name: 'AND' },
                    { name: 'OR' }
                ];

                scope.conditions = [
                    { name: '=' },
                    { name: '<>' },
                    { name: '<' },
                    { name: '<=' },
                    { name: '>' },
                    { name: '>=' },
                    { name: 'contains' },
                ];

                scope.addCondition = function () {
                    scope.query.rules.push({
                        condition: '=',
                        field: 'Firstname',
                        data: ''
                    });
                };

                scope.removeCondition = function (index) {
                    scope.query.rules.splice(index, 1);
                };

                scope.addGroup = function () {
                    scope.query.rules.push({
                        operator: 'AND',
                        rules: []
                    });
                };

                scope.removeGroup = function () {
                    "query" in scope.$parent && scope.$parent.query.rules.splice(scope.$parent.$index, 1);
                };

                directive || (directive = $compile(content));

                element.append(directive(scope, function ($compile) {
                    return $compile;
                }));
            }
        }
    }
}]);
